﻿using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Collections.Generic;
using System.Xml.XPath;
using System.Collections.ObjectModel;

namespace Resource_Grid_2
{
	public partial class Window1
	{
	    readonly ObservableCollection<DataItem> _listView1Data = new ObservableCollection<DataItem>();

        //Initialize Globals
        int _sortLast = 1;
        string _lastElementFilter = "";
        string _lastTypeFilter = "";
        string _lastSubTypeFilter = "";

		public Window1()
		{
			this.InitializeComponent();
            
			// Insert code required on object creation below this point.
            try
            {
                LoadMain(null, 0, null, null, null);
            }
            catch
            {
                MessageBox.Show("Failed loading xml");
            }
            MyListView.ItemsSource = _listView1Data;
		}

        private void LoadMain(string sortIn, XmlSortOrder sortDir, string filterIn, string typeFilterIn, string subTypeFilterIn)
        {
            MyListView.ItemsSource = null;
            MyListView.Items.Clear();
            _listView1Data.Clear();

            LoadXml(sortIn, sortDir, filterIn, typeFilterIn, subTypeFilterIn);
            MyListView.ItemsSource = _listView1Data;
        }

        private static XPathNavigator SelectFirst(XPathNavigator item, string xpath)
        {
            XPathNodeIterator iterator = item.Select(xpath);
            if (!iterator.MoveNext())
                return null;
            return iterator.Current;
        }

        private void LoadXml(string sortIn, XmlSortOrder sortDir, string resourceFilterIn, string typeFilterIn, string subTypeFilterIn)
        {
            if (string.IsNullOrEmpty(sortIn))
                sortIn = "Resource_Type";
            
            if (!cboElement.Items.Contains("All"))
                cboElement.Items.Add("All");

            if (!cboType.Items.Contains("All"))
                cboType.Items.Add("All");

            if (!cboSubType.Items.Contains("All"))
                cboSubType.Items.Add("All");

            if (sortDir == 0)
                sortDir = XmlSortOrder.Descending;

            if (string.IsNullOrEmpty(resourceFilterIn) || resourceFilterIn == "All")
            {
                resourceFilterIn = "";
                _lastElementFilter = "";
            }
            else
            {
                resourceFilterIn = "[Resource_Type='" + resourceFilterIn + "']";
            }

            if (string.IsNullOrEmpty(typeFilterIn) || typeFilterIn == "All")
            {
                typeFilterIn = "";
                _lastTypeFilter = "";
            }
            else
            {
                typeFilterIn = "[Sub_Type='" + typeFilterIn + "']";
            }

            if (string.IsNullOrEmpty(subTypeFilterIn) || subTypeFilterIn == "All")
            {
                subTypeFilterIn = "";
                _lastSubTypeFilter = "";
            }
            else
            {
                subTypeFilterIn = "[Description='" + subTypeFilterIn + "']";
            }

            var docNav = new XPathDocument("Resources.xml");
            XPathNavigator navigator = docNav.CreateNavigator();
            Console.WriteLine("Full Select: //Resource" + resourceFilterIn + typeFilterIn + subTypeFilterIn);
            XPathExpression expression = navigator.Compile("//Resource" + resourceFilterIn + typeFilterIn + subTypeFilterIn);
            expression.AddSort(sortIn, sortDir, XmlCaseOrder.None,
                string.Empty, XmlDataType.Number);
            XPathNodeIterator node = navigator.Select(expression);

            for (int q = 0; q < node.Count; q++)
            {
                node.MoveNext();
                var item = new DataItem();
                var strArray = new string[] 
                        {
                                item.ResName = SelectFirst(node.Current, "Name").Value,
                                item.ResPlanet = SelectFirst(node.Current, "Planet").Value,
                                item.ResCR = SelectFirst(node.Current, "CR").Value,
                                item.ResCD = SelectFirst(node.Current, "CD").Value,
                                item.ResDR = SelectFirst(node.Current, "DR").Value,
                                item.ResFL = SelectFirst(node.Current, "FL").Value,
                                item.ResHR = SelectFirst(node.Current, "HR").Value,
                                item.ResMA = SelectFirst(node.Current, "MA").Value,
                                item.ResPE = SelectFirst(node.Current, "PE").Value,
                                item.ResOQ = SelectFirst(node.Current, "OQ").Value,
                                item.ResSR = SelectFirst(node.Current, "SR").Value,
                                item.ResUT = SelectFirst(node.Current, "UT").Value,
                                item.ResER = SelectFirst(node.Current, "ER").Value,
                                item.ResResource_Type = SelectFirst(node.Current, "Resource_Type").Value,
                                item.ResSub_Type = SelectFirst(node.Current, "Sub_Type").Value,
                                item.ResDescription = SelectFirst(node.Current, "Description").Value

                        };
                if (!cboElement.Items.Contains(item.ResResource_Type))
                {
                    cboElement.Items.Add(item.ResResource_Type);
                }

                if (!cboType.Items.Contains(item.ResSub_Type))
                    cboType.Items.Add(item.ResSub_Type);

                if (!cboSubType.Items.Contains(item.ResDescription))
                    cboSubType.Items.Add(item.ResDescription);

                _listView1Data.Add(item);
                MyListView.Items.Refresh();

            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void MainBackground_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            var client = new WebClient();
            try
            {
                client.DownloadFile("http://www.jcnash.com/files/Resources.xml", "Resources.xml");
                MessageBox.Show("XML Downloaded Successfully");

                MyListView.ItemsSource = null;
                _listView1Data.Clear();

                LoadMain(null, 0, null, null, null);
                MyListView.ItemsSource = _listView1Data;
            }
            catch
            {
                MessageBox.Show("Error Downloading XML");
            }
        }

        private void cboElement_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _lastElementFilter = cboElement.SelectedValue.ToString();

            LoadMain(null, XmlSortOrder.Descending, Convert.ToString(cboElement.Items[cboElement.SelectedIndex]),
                     _lastTypeFilter, _lastSubTypeFilter);
        }

        private void cboType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _lastTypeFilter = cboType.SelectedItem.ToString();

            LoadMain(null, XmlSortOrder.Descending, _lastElementFilter,
                     Convert.ToString(cboType.Items[cboType.SelectedIndex]), _lastSubTypeFilter);

        }

	    private void cboSubType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _lastSubTypeFilter = cboSubType.SelectedItem.ToString();

            LoadMain(null, XmlSortOrder.Descending, _lastElementFilter, _lastTypeFilter,
                     Convert.ToString(cboSubType.Items[cboSubType.SelectedIndex]));
        }

	    private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            cboElement.Items.Clear();
            cboType.Items.Clear();
            cboSubType.Items.Clear();
            MyListView.ItemsSource = null;
            _listView1Data.Clear();
            _lastElementFilter = "All";
            _lastTypeFilter = "All";
            _lastSubTypeFilter = "All";
            cboElement.Text = "All";
            cboType.Text = "All";
            cboSubType.Text = "All";

            LoadMain(null, 0, null, null, null);
            
        }

        //private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        //{

        //    dataGridView1.DataSource = null;
        //    m_Resource.Clear();

        //    if (SortLast == 0)
        //    {
        //        LoadMain(listforxml[e.ColumnIndex], XmlSortOrder.Ascending, LastResourceFilter, LastTypeFilter, LastSubTypeFilter);
        //        Console.WriteLine("Sorting By: " + listforxml[e.ColumnIndex]);
        //        SortLast = 1;
        //    }
        //    else
        //    {
        //        LoadMain(listforxml[e.ColumnIndex], XmlSortOrder.Descending, LastResourceFilter, LastTypeFilter, LastSubTypeFilter);
        //        Console.WriteLine("Sorting By: " + listforxml[e.ColumnIndex]);
        //        SortLast = 0;
        //    }
        //}

        //private void btnAddResource_Click(object sender, EventArgs e)
        //{
        //    string Planets = "";
        //    for (int i = 0; i < chklbInPlanet.CheckedItems.Count; i++)
        //    {
        //        if (i != chklbInPlanet.CheckedItems.Count - 1)
        //            Planets += chklbInPlanet.CheckedItems[i] + ", ";
        //        else
        //            Planets += chklbInPlanet.CheckedItems[i];
        //    }
        //    MessageBox.Show("TESTING MODE - Text In"
        //        + Environment.NewLine
        //        + "Name:" + txtInName.Text
        //        + Environment.NewLine
        //        + "Planet:" + Planets
        //        + Environment.NewLine
        //        + "CR:" + txtInCR.Text
        //        + Environment.NewLine
        //        + "CD:" + txtInCD.Text
        //        + Environment.NewLine
        //        + "DR:" + txtInDR.Text
        //        + Environment.NewLine
        //        + "FL:" + txtInFL.Text
        //        + Environment.NewLine
        //        + "HR:" + txtInHR.Text
        //        + Environment.NewLine
        //        + "MA:" + txtInMA.Text
        //        + Environment.NewLine
        //        + "PE:" + txtInPE.Text
        //        + Environment.NewLine
        //        + "OQ:" + txtInOQ.Text
        //        + Environment.NewLine
        //        + "SR:" + txtInSR.Text
        //        + Environment.NewLine
        //        + "UT:" + txtInUT.Text
        //        + Environment.NewLine
        //        + "ER:" + txtInER.Text
        //        + Environment.NewLine
        //        + "Element:" + cboInElement.SelectedValue
        //        + Environment.NewLine
        //        + "Type:" + txtInType.Text
        //        + Environment.NewLine
        //        + "Subtype:" + txtInSubType.Text);

        //    using (XmlWriter writer = XmlWriter.Create("Resources.xml"))
        //    {
        //        writer.WriteStartElement("Resources");

        //        writer.WriteStartElement("Resource");

        //        writer.WriteElementString("Name", txtInName.Text);
        //        writer.WriteElementString("Description", txtInSubType.Text);
        //        writer.WriteElementString("Planet", Planets);
        //        writer.WriteElementString("CR", txtInCR.Text);
        //        writer.WriteElementString("CD", txtInCD.Text);
        //        writer.WriteElementString("DR", txtInDR.Text);
        //        writer.WriteElementString("FL", txtInFL.Text);
        //        writer.WriteElementString("HR", txtInHR.Text);
        //        writer.WriteElementString("MA", txtInMA.Text);
        //        writer.WriteElementString("PE", txtInPE.Text);
        //        writer.WriteElementString("OQ", txtInOQ.Text);
        //        writer.WriteElementString("SR", txtInSR.Text);
        //        writer.WriteElementString("UT", txtInUT.Text);
        //        writer.WriteElementString("ER", txtInER.Text);
        //        writer.WriteElementString("Resource_Type", cboInElement.SelectedText);
        //        writer.WriteElementString("Sub_Type", txtInType.Text);

        //        writer.WriteEndElement();
        //        writer.WriteEndElement();

        //    }

        //}
	}
}