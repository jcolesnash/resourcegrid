﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Resource_Grid_2
{
    class DataItem
    {
        public string ResName { get; set; }
        public string ResPlanet { get; set; }
        public string ResCR { get; set; }
        public string ResCD { get; set; }
        public string ResDR { get; set; }
        public string ResFL { get; set; }
        public string ResHR { get; set; }
        public string ResMA { get; set; }
        public string ResPE { get; set; }
        public string ResOQ { get; set; }
        public string ResSR { get; set; }
        public string ResUT { get; set; }
        public string ResER { get; set; }
        public string ResResource_Type { get; set; }
        public string ResSub_Type { get; set; }
        public string ResDescription { get; set; }
    }
}
